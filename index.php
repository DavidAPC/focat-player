<html>
<?php
  $PageEncoding = 'UTF-8';
  require_once('getid3/getid3.php');
  $getID3 = new getID3;
  $getID3->setOption(array('encoding' => $PageEncoding));
 ?>
<head>
    <meta charset="utf-8">
    <title>Focat Player</title>

    <style type="text/css">
        body {
          /*  padding: 0;
            margin: 0;
            border: 0;*/
            font-family: Helvetica, Fantasy, arial;
        }
        #mainPane{
          margin:auto;
          max-width:1000px;
        }
        .headerButton{
          float:right;
          padding-left:10px;
          padding-right:10px;
          cursor: pointer;
        }
        .header span{
          font-weight: bold;
          cursor:pointer;
        }
        #player{
          padding-top:13px;
          float:right;
        }
        .entry:nth-child(even) {background: #F2F0F0}
        .entry:nth-child(odd) {background: #FFF}

        #entries .entry.playing{
          background-color:#f5f5c8;
        }

        .entry{
          cursor: pointer;
        }
        .artist{
          width:25%;
        }
        .title{
          width:25%;
        }
        .album{
          width:40%;
        }
        .playtime{
          width:10%;
        }
        span{
          display:inline-block;
        }
        .filename{
          display:none;
        }
        .playtime{
          float:right;
        }
    </style>
    <!--<link rel="stylesheet" href="style.css">-->
    <script type="text/javascript" src="scripts/jquery-1.12.3.js"></script>
    <script>
        function play(elem){
          var filename = $(".filename",elem).text();
          console.log("entry clicked" + filename)
          if($("#audioSource").length==0)
            $("#player").append('<source id="audioSource" src="'+filename+'" type="audio/mpeg">')
          else
            $("#audioSource").attr("src", filename)
          var player = $("#player")[0]
          player.load();
          player.play();
          $(".entry").removeClass("playing")
          $(elem).addClass("playing");
        }
        $(function() {
          $("#entries").on("click", ".entry", function(){
            play(this);
            //.hide()
          });
          $("#player")[0].addEventListener("ended", function() {
            var next = $(".playing + div.entry");
            $(".entry").removeClass("playing")
            if(next.length==1){
              play(next[0])
            }
          });
          $("#player")[0].addEventListener("pause", function() {
            $("#playButton").attr("src", "images/playButton.png")
          });
          $("#player")[0].addEventListener("play", function() {
            $("#playButton").attr("src", "images/pauseButton.png")
          });
          $("#playButton").click(function(){
            var player = $("#player")[0]
            if(player.paused){
              if($("#audioSource").length==0){
                play($(".entry:first")[0]);
              } else {
                player.play();
              }
            }
            else {
              player.pause();
            }
          });
          $(".header span").click(function(){
            var headerType = $(this).attr("class");
            var sortedEntries = $(".entry").sort(function(a,b){
              var ae = $("."+headerType, a).text().toLowerCase()
              var be = $("."+headerType, b).text().toLowerCase()
              var result = 0;
              if(ae > be) {
                  result = 1;
              } else if(ae < be) {
                  result = -1;
              }
              return result;
            });
            $("#entries").html(sortedEntries)
          });
        });
    </script>
</head>
  <div id="mainPane">
    <div id="header">
      <div id="playerSection">
        <audio id="player" controls>

       Your browser does not support the audio element.
       </audio>
         <img id="playButton" class="headerButton" src="images/playButton.png" />
      </div>
      <h1>Focat Player</h1>
    </div>
    <div id="sidebar"></div>
    <div id="main">
      <div class='header'>
        <span class='title'>Title</span><span class='artist'>Artist</span><span class='album'>Album</span><span class='playtime'>Playtime</span>
      </div>
      <div id="entries">
      <?php
        function endsWith($haystack, $needle) {
            // search forward starting from end minus needle length characters
            return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
        }
        function processFile($filePath){
          global $getID3;
          if(endsWith($filePath,".mp3") || endsWith($filePath,".ogg")){
            //print_r($tags);
            $ThisFileInfo = $getID3->analyze($filePath);
            getid3_lib::CopyTagsToComments($ThisFileInfo);
            $title =  $ThisFileInfo['comments_html']["title"][0];
            if(empty($title)){
              $title = $filename;
            }
            $playtime =  $ThisFileInfo['playtime_string'];
            $album =  $ThisFileInfo['comments_html']["album"][0];
            $artist =  $ThisFileInfo['comments_html']["artist"][0];
            echo "<div class='entry'><span class='filename'>$filePath</span><span class='title'>$title</span><span class='artist'>$artist</span><span class='album'>$album</span><span class='playtime'>$playtime</span></div>";
          } else if($filePath!==".." && $filePath!="." && is_dir($filePath)){
              $directoryListing = scandir($filePath);
              foreach($directoryListing as $filename){
                if($filename!="." && $filename!=".."){
                  $directoryFilePath = $filePath.'/'.$filename;
                  processFile($directoryFilePath);
                }
              }
          };
        }

        $music_directory = "Music";
        processFile($music_directory);


       ?>
       </div>
    </div>
  </div>
<body>


</body>

</html>
